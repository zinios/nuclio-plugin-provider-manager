Release Notes
-------------
1.1.0
-----
* searchForProviders is now compatible with symlinks.
* Using new temp path constant.

1.0.0
-----
* Initial Release.