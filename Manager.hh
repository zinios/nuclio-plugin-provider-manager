<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\provider\manager
{
	use nuclio\core\ClassManager;
	use nuclio\core\plugin\Plugin;
	use nuclio\helper\ObjectHelper;
	use nuclio\plugin\system\process\Process;
	use \RecursiveIteratorIterator;
	use \RecursiveDirectoryIterator;

	<<singleton>>
	class Manager extends Plugin
	{
		private Vector<string> $paths;
		private Map<string,Vector<string>> $providers;
		private bool $loaded=false;

		public static function getInstance(/* HH_FIXME[4033] */...$args):Manager
		{
			$instance=ClassManager::getClassInstance(self::class);
			return ($instance instanceof self)?$instance:new self();
		}

		public function __construct()
		{
			parent::__construct();

			$this->paths	=new Vector(null);
			$this->providers=new Map(null);
		}

		public static function request(string $provider,/* HH_FIXME[4033] */...$args):mixed
		{
			$self=self::getInstance();
			if (!$self->loaded)
			{
				$self->loadProviders();
			}
			if ($self->providers->containsKey($provider))
			{
				$providers=$self->providers->get($provider);
				if (!is_null($providers) && $providers->containsKey(0))
				{
					$providers	=$self->sortHighestPriority($providers);
					$providerDef=$providers->get(0);
					$className	=$providerDef->get('class');
					if (is_string($className))
					{
						$instance=ClassManager::getInstance()->createInstance(ClassManager::DEFAULT_CONTAINER,$className,...$args);
						return $instance;
					}
				}
			}
			return null;
		}
		
		private function sortHighestPriority(Vector<array<string,string>> $providers):Vector<Map<string,int>>
		{
			$return		=Vector{};
			$class		=[];
			$priority	=[];
			foreach ($providers as $i=>$provider)
			{
				$class[$i]		=$provider['class'];
				$priority[$i]	=$provider['priority'];
			}
			asort(&$priority,SORT_NUMERIC);
			$priority=array_reverse($priority);
			foreach ($priority as $i=>$provider)
			{
				$return->add(new Map($providers[$i]));
			}
			return $return;
		}

		public function loadProviders():this
		{
			$cacheFile=NUC_TMP_DIR.'providers.map';
			if (!file_exists($cacheFile))
			{
				$message=<<<MSG
Providers have not been mapped<br>
You can map them with the nuclio command line tool.<br>
Use the "providers::map" command.
MSG;
				throw new ManagerException($message);
			}
			if (!is_readable($cacheFile))
			{
				throw new ManagerException(sprintf('Provider cache file "%s" is not readable.',$cacheFile));
			}
			$contents=json_decode(file_get_contents($cacheFile),true);
			if (!is_null($contents))
			{
				$this->providers=new Map($contents);
				foreach ($this->providers as $provider=>$classes)
				{
					$this->providers[$provider]=new Vector($classes);
				}
			}
			$this->loaded=true;
			return $this;
		}

		public function bindPaths(Vector<string> $paths):this
		{
			foreach ($paths as $path)
			{
				$this->paths[]=$path;
			}
			return $this;
		}

		public function searchForProviders(bool $consoleOutput=false):Map<string,Vector<string>>
		{
			$providerMap=new Map(null);
			$nuclioExec	='hhvm '.realpath(NUC_BIN);
			foreach ($this->paths as $path)
			{
				$results=null;
				chdir($path);
				exec('grep -R --include="*.hh" "provides("',&$results);
				print "Mapping providers in \"\033[0;33m".$path."\033[0m\"".PHP_EOL;
				foreach ($results as $result)
				{
					list($filePath)=explode(':',$result);
					if ($filePath===__FILE__)
					{
						continue;
					}
					print "\033[0;32mFound Providers in ".realpath($filePath)."\033[0m".PHP_EOL;
					$process=Process::getInstance
					(
						$nuclioExec,
						Vector
						{
							'providers::reflect',
							// '--silent',
							'--file '.realpath($filePath)
						}
					);
					$output=$process->getOutput();
					if ($output!=='' && !stristr($output,'error') && !stristr($output,'exception'))
					{
						$output		=unserialize($output);
						$providers	=new Vector($output[1]);
						$last		=$providers->lastValue();
						$priority	=0;
						if (is_int($last))
						{
							$priority=(int)$last;
							$providers->pop();
						}
						if ($providers->count())
						{
							foreach ($providers as $provider)
							{
								if (!$providerMap->containsKey($provider))
								{
									$providerMap->set($provider,Vector{});
								}
								$providerVector=$providerMap->get($provider);
								$providerVector->add
								(
									Map
									{
										'class'		=>$output[0],
										'priority'	=>$priority
									}
								);
								$providerMap->set($provider,$providerVector);
							}
							if ($consoleOutput)
							{
								print "Found these providers:".PHP_EOL;
								foreach ($providers as $provider)
								{
									print "\033[0;34m * \033[0;96m".$provider." [".$priority."]\033[0".PHP_EOL;
								}
							}
						}
					}
					else if ($consoleOutput)
					{
						print "\033[0;31mWarning! File is invalid. Provider will not be available from ".realpath($filePath)."\033[0m".PHP_EOL;
						if (strlen($output))
						{
							print "\033[0;33mI was able to capture the following error:\033[0m".PHP_EOL;
							print "\033[1;41m".PHP_EOL.PHP_EOL.trim($output).PHP_EOL."\033[0m".PHP_EOL.PHP_EOL;
						}
					}
				}
			}
			return $providerMap;
		}
	}
}
